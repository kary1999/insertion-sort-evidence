package app;

import java.io.PrintWriter;

import com.panayotis.gnuplot.JavaPlot;

public class App {
	public static void main(String[] args){

	    /* Setup */
	    final int TOTAL_NUMBER_OF_CASES = 2000;
	    
	    InsertionSort insertionSort = new InsertionSort();
	    final String fileName = "output.txt";
	
	    try{
	      PrintWriter fs = new PrintWriter(fileName);
	      for(int i = 1; i <= TOTAL_NUMBER_OF_CASES; i ++){
	        double[] worstCase = new double[i];
	        for(int j = 0; j < i; j ++){
	          worstCase[j] = i - j;
	        }
	        Long time = insertionSort.sort(worstCase);
	        fs.println(i + " " + time);
	      }
	      fs.close();
	      
	      JavaPlot p = new JavaPlot();
	      String source = "\"/home/karolyto/Documentos/Vsemestre/LP3/laboratorio/lp321/" + fileName + "\"";
	      p.addPlot(source + " with lines");
	      p.plot();
	    }catch(Exception e){
	      System.out.println(e.getMessage());
	    }
	}
}
