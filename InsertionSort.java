package app;

public class InsertionSort {
	/* Public Methods */
	public Long sort(double arr[]){
    /* Function to sort array using insertion sort */
    final int LENGTH = arr.length;
		Long start = System.nanoTime();
		for(int i = 1; i < LENGTH; i ++){
			double key = arr[i];
			int j = i - 1;

			/* 
		Mover elementos de arr [0..i-1], que son
			mayor que la clave, a una posición por delante
	de su posición actual */
			while(j >= 0 && arr[j] > key){
				arr[j + 1] = arr[j];
        j --;
			}
			arr[j + 1] = key;
		}
    	return System.nanoTime() - start;
	}
}
