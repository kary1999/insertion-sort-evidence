package app;

public class Utils {
	/* Public Methods */
	public void printArray(double arr[]){
		/* A utility function to print array */
		for (int i = 0; i < arr.length; i ++){
      System.out.println(arr[i]);
    }
	}

	public double getRandomArbitrary(int min, int max){
		/* Returns a random number between min (included) and max (excluded) */
		return Math.random() * (max - min) + min;
	}
}
